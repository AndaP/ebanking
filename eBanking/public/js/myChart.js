$(document).ready(function () {
    getDataAndShowChart()
});

/**
 * Get All Transaction Data
 * @returns {string}
 */
function getDataAndShowChart() {
    $.get("http://127.0.0.1:8000/transactions", function (data) {
        showChart(data);
    });
}
/**
 * This function create a new Chart Line diagram
 * @param data JSON from REST
 */
function showChart(data) {
    var ctx = document.getElementById("myChart");
    var einnahmen = 0;
    var ausgaben = 0;
    $.each(data, function (index, value) {
                if(value.value > 0){
                    einnahmen += value.value;
                }else{
                    ausgaben += value.value * -1;
                }
    });

    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ['Eingaben', 'Ausgaben'],
            datasets: [{
                label: 'Ein- und Ausgaben',
                data: [einnahmen, ausgaben],
                backgroundColor: [
                    'green',
                    'red'
                ],
                borderColor: [
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 99, 132, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
}



