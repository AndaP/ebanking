<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $fillable = [
        'account_number',
        'bank_balance',
        'iban',
        'bic',
        'user_id'
    ];


    public function transactionsPositive(Account $account){
        return Transaction::with(['origin'])->where('receiver_id', '=', $account->id)->get();
    }

    public function transactionsNegative(Account $account){
        return Transaction::with(['origin'])->where('origin_id', '=', $account->id)->get();
    }

    public function transactions(Account $account){
        return Transaction::with(['origin'])->where('receiver_id', '=', $account->id)->orWhere('origin_id', '=', $account->id)->orderByDesc('created_at')->get();

    }
}
