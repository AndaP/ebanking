<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Transaction extends Model
{
    protected $fillable = [
        'purpose',
        'reference',
        'value',
        'origin_id',
        'receiver_id'
    ];

    public function origin(){
        return $this->belongsTo(Account::class, 'origin_id');
    }

    public function receiver(){
        return $this->belongsTo(Account::class, 'receiver_id');
    }

    public static function getByReference($account_id, $reference){

         return Transaction::where('reference', 'LIKE', '%' . $reference . '%')
                         ->where(function ($query) use (&$account_id){
                             $query->where('receiver_id', '=', $account_id)
                                 ->orwhere('origin_id', '=', $account_id);
                         })->get();

    }

    public static function getByPurpose($account_id, $purpose){
        return Transaction::where('purpose', 'LIKE', '%' . $purpose . '%')
            ->where(function ($query) use (&$account_id){
                $query->where('receiver_id', '=', $account_id)
                    ->orwhere('origin_id', '=', $account_id);
            })->get();
    }

    public static function getByDate($account_id, $startDate, $endDate){
        return Transaction::whereBetween('created_at', array($startDate, $endDate))
            ->where(function ($query) use (&$account_id){
                $query->where('receiver_id', '=', $account_id)
                    ->orwhere('origin_id', '=', $account_id);
            })->get();
    }

    public static function getByValue($account_id, $startValue, $endValue){
        return Transaction::whereBetween('value', array($startValue, $endValue))
            ->where(function ($query) use (&$account_id){
                $query->where('receiver_id', '=', $account_id)
                    ->orwhere('origin_id', '=', $account_id);
            })->get();
    }


}
