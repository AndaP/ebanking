<?php

namespace App\Providers;

use App\Account;
use Illuminate\Support\ServiceProvider;

class AccountServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

        $this->app->singleton('iban_bic', function (){

            $check = false;
            $iban = env('BANK_IBAN');

            while ($check == false){
                while (strlen($iban) < 20){
                    $iban .= mt_rand(0,9);
                }

                $all_accounts = Account::all();
                if(!$all_accounts->isEmpty()){
                    foreach ($all_accounts AS $account){
                        if($iban==$account->iban){
                            $check = false;
                        }else{
                            $check = true;
                        }
                    }
                }
                else{
                    $check = true;
                }
            }
            return [
              'iban'    => $iban,
              'bic'     => env('BANK_BIC')
            ];
        });
    }
}
