<?php

namespace App\Http\Controllers;

use App\Account;
use App\Transaction;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use PDF;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        $account = Account::where('user_id', '=', $user->id)->first();
        return view('transaction.create', [
            'origin_iban' => $account->iban
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validated = $this->validate(request(), [
            'receiver_iban' => [function ($attribute, $value, $fail){
                $receiver = Account::where('iban', '=', $value);
                if($receiver == null){
                    $fail(': Kein Bankkonto mit diesem IBAN vorhanden');
                }
            }],
            'purpose' => 'required|max:255',
            'reference' => 'required|max:255',
            'value' => 'required|min:1'
        ]);

        $receiver = Account::where('iban', '=', $request->receiver_iban)->first();
        $origin = Account::where('iban', '=', $request->origin_iban)->first();

        $receiver->bank_balance += $request->value;
        $origin->bank_balance -= $request->value;

        $receiver->save();
        $origin->save();

        $validated['receiver_id']=$receiver->id;
        $validated['origin_id']=$origin->id;

        Transaction::create($validated);

        $this->generatePdf($validated);

        return redirect('/home');

    }

    private function generatePdf($transactionData){
        $time = Carbon::now()->toDateString();
        $transactionData['time'] = $time;
        $data = [
            $time,
            $transactionData
        ];
        $pdf = PDF::loadView('pdf.beleg', $transactionData);
        $pdf->download('beleg.pdf');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction)
    {
        //DB::enableQueryLog();
        $user = Auth::user();
        $account = Account::where('user_id', '=', $user->id)->first();
        $transactions = $account->transactions($account);

        return view('transaction.show', [
            'transactions'  => $transactions
        ]);
    }

    public function transactionsPositive(){
        $transactions = [];
        $user = Auth::user();
        $account = Account::where('user_id', '=', $user->id)->first();
        $accountTransactionsPositive = $account->transactionsPositive($account);
        foreach ($accountTransactionsPositive AS $item){
            array_push($transactions, $item);
        }
        return json_encode($transactions);

    }

    public function transactionsNegative(){
        $transactions = [];
        $user = Auth::user();
        $account = Account::where('user_id', '=', $user->id)->first();
        $accountTransactionsNegative = $account->transactionsNegative($account);
        foreach ($accountTransactionsNegative AS $item){
            array_push($transactions, $item);
        }
        return json_encode($transactions);

    }

    public function getAll()
    {
        $user = Auth::user();
        $account = Account::where('user_id', '=', $user->id)->first();
        $transactions = $account->transactions($account);
        foreach ($transactions AS $transaction) {
            if ($transaction->origin_id == $account->id) {
                $transaction->value = $transaction->value * -1;
            }
            $transaction->origin_name = User::where('id', '=', Account::where('id', '=', $transaction->origin_id)->first()->user_id)->first()->name;
            $transaction->receiver_name = User::where('id', '=', Account::where('id', '=', $transaction->receiver_id)->first()->user_id)->first()->name;
        }
        return $transactions;
    }

    public function getByReference(Request $request){

        $reference = $this->validate($request, [
            'reference' => 'required|max:255'
        ]);
        $user = Auth::user();
        $account = Account::where('user_id', '=', $user->id)->first();

        $transactions = Transaction::getByReference($account->id, $reference['reference']);
        foreach ($transactions AS $transaction){
            if($transaction->origin_id == $account->id){
                $transaction->value = $transaction->value * -1;
            }
            $transaction->origin_name = User::where('id', '=', Account::where('id', '=', $transaction->origin_id)->first()->user_id)->first()->name;
            $transaction->receiver_name = User::where('id', '=', Account::where('id', '=', $transaction->receiver_id)->first()->user_id)->first()->name;
        }

        return view('accounts.index', [
            'user'          => $user,
            'account'       => $account,
            'iban'          => chunk_split($account->iban, 4, ' '),
            'bic'           => $account->bic,
            'transactions'  => $transactions
        ]);

    }

    public function getByPurpose(Request $request){

        $reference = $this->validate($request, [
            'purpose' => 'required|max:255'
        ]);
        $user = Auth::user();
        $account = Account::where('user_id', '=', $user->id)->first();

        $transactions = Transaction::getByPurpose($account->id, $reference['purpose']);
        foreach ($transactions AS $transaction){
            if($transaction->origin_id == $account->id){
                $transaction->value = $transaction->value * -1;
            }
            $transaction->origin_name = User::where('id', '=', Account::where('id', '=', $transaction->origin_id)->first()->user_id)->first()->name;
            $transaction->receiver_name = User::where('id', '=', Account::where('id', '=', $transaction->receiver_id)->first()->user_id)->first()->name;
        }

        return view('accounts.index', [
            'user'          => $user,
            'account'       => $account,
            'iban'          => chunk_split($account->iban, 4, ' '),
            'bic'           => $account->bic,
            'transactions'  => $transactions
        ]);

    }

    public function getByDate(){

        $validated = $this->validate(request(), [
            'start_date' => 'required|date_format:Y-m-d|before_or_equal:end_date',
            'end_date' => 'required|date_format:Y-m-d|after_or_equal:start_date'
        ]);

        $start = $validated['start_date'];
        $end = $validated['end_date'];
        $user = Auth::user();
        $account = Account::where('user_id', '=', $user->id)->first();

        $transactions = Transaction::getByDate($account->id, $start, $end);
        foreach ($transactions AS $transaction){
            if($transaction->origin_id == $account->id){
                $transaction->value = $transaction->value * -1;
            }
            $transaction->origin_name = User::where('id', '=', Account::where('id', '=', $transaction->origin_id)->first()->user_id)->first()->name;
            $transaction->receiver_name = User::where('id', '=', Account::where('id', '=', $transaction->receiver_id)->first()->user_id)->first()->name;
        }

        return view('accounts.index', [
            'user'          => $user,
            'account'       => $account,
            'iban'          => chunk_split($account->iban, 4, ' '),
            'bic'           => $account->bic,
            'transactions'  => $transactions
        ]);

    }

    public function getByValue(){


        $validated = $this->validate(request(), [
            'start' => 'required|numeric|lt:end',
            'end' => 'required|numeric|gt:start'
        ]);
        $start = $validated['start'];
        $end = $validated['end'];
        $user = Auth::user();

        $account = Account::where('user_id', '=', $user->id)->first();

        $transactions = Transaction::getByValue($account->id, $start, $end);
        foreach ($transactions AS $transaction){
            if($transaction->origin_id == $account->id){
                $transaction->value = $transaction->value * -1;
            }
            $transaction->origin_name = User::where('id', '=', Account::where('id', '=', $transaction->origin_id)->first()->user_id)->first()->name;
            $transaction->receiver_name = User::where('id', '=', Account::where('id', '=', $transaction->receiver_id)->first()->user_id)->first()->name;
        }

        return view('accounts.index', [
            'user'          => $user,
            'account'       => $account,
            'iban'          => chunk_split($account->iban, 4, ' '),
            'bic'           => $account->bic,
            'transactions'  => $transactions
        ]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction $transaction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaction $transaction)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaction $transaction)
    {
        //
    }
}
