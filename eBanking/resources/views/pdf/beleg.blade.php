<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="utf-8" />
    <title>Titel</title>
</head>
<body>

<h2 align="center">Beleg für Einzahlung am {{$time}}</h2>

<p><strong>IBAN des Empfängers</strong></p>
{{$receiver_iban}}

<p><strong>Eingezahlter Betrag</strong></p>
{{$value}}

<p><strong>Verwendugszweck</strong></p>
{{$purpose}}

<p><strong>Referenz</strong></p>
{{$reference}}


</body>
</html>