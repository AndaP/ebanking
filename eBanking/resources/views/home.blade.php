@extends('layouts.app')
@section('title', 'Home')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <p>You are logged in!</p>

                        <a href="/accounts"><button class="btn-primary">My Account</button></a>
                        <a href="/transaction/create"><button class="btn-success">New Transaction</button> </a>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection
