@extends('layouts.app')
@section('title', 'Transaction')
@section('content')
    <div class="form-group">
        <form method="POST" action="/transaction">
            @csrf
            <input type="hidden" id="origin_iban" name="origin_iban" value="{{$origin_iban}}">

            <div class="form-group row">
                <label for="purpose" class="col-md-4 col-form-label text-md-right">{{ __('Verwendungszweck') }}</label>

                <div class="col-md-6">
                    <input id="purpose" type="text" class="form-control @error('purpose') is-invalid @enderror" name="purpose" value="{{ old('purpose') }}" required autofocus>

                    @error('purpose')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
            </div>
            <div class="form-group row">
                <label for="reference" class="col-md-4 col-form-label text-md-right">{{ __('Verweis') }}</label>

                <div class="col-md-6">
                    <input id="reference" type="text" class="form-control @error('reference') is-invalid @enderror" name="reference" value="{{ old('reference') }}" required>

                    @error('reference')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
            </div>
            <div class="form-group row">
                <label for="reference" class="col-md-4 col-form-label text-md-right">{{ __('Betrag') }}</label>

                <div class="col-md-6">
                    <input id="value" type="number" class="form-control @error('value') is-invalid @enderror" name="value" value="{{ old('value') }}" required>

                    @error('value')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
            </div>
            <div class="form-group row">
                <label for="receiver_iban" class="col-md-4 col-form-label text-md-right">{{ __('IBAN vom Empfänger') }}</label>

                <div class="col-md-6">
                    <input id="receiver_iban" type="text" class="form-control @error('receiver_iban') is-invalid @enderror" name="receiver_iban" value="{{ old('receiver_iban') }}" required>

                    @error('receiver_iban')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
            </div>
            <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                    <button type="submit" class="btn btn-primary">
                        {{ __('Überweisen') }}
                    </button>
                </div>
            </div>
        </form>
    </div>
@endsection

