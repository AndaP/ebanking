@extends('layouts.app')
@section('title', 'My Account')
@section('content')
    <div class="row">
        <div class="col">
            <div class="card" style="width: 22rem">
                <div class="card-header">
                    Finanzübersicht
                </div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                        <div class="row">
                            <div class="col-sm-8">
                                <strong>{{$user->name}}</strong>
                                <p>Giro</p>
                            </div>
                            <div class="col-sm-4">
                                @if($account->bank_balance >= 0)
                                    <div class="balance-positive">
                                        {{number_format($account->bank_balance, 2, ',', '.')}}
                                    </div>
                                @else
                                    <div class="balance-negative">
                                        {{number_format($account->bank_balance, 2, ',', '.')}}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <br>
            <br>
            <div class="card">
                <div class="card-header">
                    <strong>Eingaben und Ausgaben</strong>
                </div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                        <div class="row">
                            <canvas id="myChart" width="auto" height="auto"></canvas>
                        </div>
                    </li>
                </ul>
            </div>
            <br>
            <br>
            <div id="searchByReference">
                <form method="GET" action="/transactions/reference">
                    @csrf
                    <label for="reference">
                        Suche nach Referenz:
                    </label><br>
                    <input id="reference" name="reference" type="text" class="form-control @error('reference') is-invalid @enderror">
                    @error('reference')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                    <button type="submit" class="btn-primary">Suche</button>
                </form>
            </div><hr>
            <div id="searchByPurpose">
                <form method="GET" action="/transactions/purpose">
                    @csrf
                    <label for="purpose">
                        Suche nach Verwendungsweck:
                    </label><br>
                    <input id="purpose" name="purpose" type="text" class="form-control @error('purpose') is-invalid @enderror">
                    @error('purpose')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                    <button type="submit" class="btn-success">Suche</button>
                </form>
            </div><hr>
            <div id="searchByDate">
                <form method="GET" action="/transactions/date">
                    @csrf
                    <label for="start_date">
                        Start Datum:
                    </label><br>
                    <input id="start_date" name="start_date" type="date" class="form-control @error('start_date') is-invalid @enderror">
                    @error('start_date')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                    <br>
                    <label for="end_date">
                        End Datum:
                    </label><br>
                    <input id="end_date" name="end_date" type="date" class="form-control @error('end_date') is-invalid @enderror">
                    @error('end_date')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                    <button type="submit" class="btn-dark">Suche</button>
                </form>
            </div><hr>

            <div id="searchByValue">
                <form method="GET" action="/transactions/value">
                    @csrf
                    <label for="start">
                        Start Betrag:
                    </label><br>
                    <input id="start" name="start" type="number" class="form-control @error('start') is-invalid @enderror">
                    @error('start')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                    <br>
                    <label for="end">
                        End Betrag:
                    </label><br>
                    <input id="end" name="end" type="number" class="form-control @error('end') is-invalid @enderror">
                    @error('end')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                    <button type="submit" class="btn-dark">Suche</button>
                </form>
            </div><hr>
        </div>
        <div class="col">
            <div class="card" style="width: 42rem">
                <div class="card-header">
                    <p>Umsätze</p>
                    <p><strong style="text-transform: uppercase">{{$user->name}}</strong></p>
                    <p>IBAN {{$iban}}</p>
                    <p>BIC {{$bic}}</p>
                </div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                        @foreach($transactions AS $transaction)
                            <div class="row">
                                <div class="col-sm-5">
                                    {{$transaction->created_at}}
                                </div>
                                <div class="col-sm-5">
                                    @if($transaction->value < 0)
                                        {{$transaction->receiver_name}}
                                    @else
                                        {{$transaction->origin_name}}
                                    @endif
                                    <br>
                                    {{$transaction->purpose}}
                                </div>
                                <div class="col-sm-2">
                                    @if($transaction->value > 0)
                                        <div class="balance-positive">
                                            {{number_format($transaction->value, 2, ',', '.')}}
                                        </div>
                                    @else
                                        <div class="balance-negative">
                                            {{number_format($transaction->value, 2, ',', '.')}}
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <hr>
                        @endforeach
                    </li>
                </ul>
            </div>
        </div>
    </div>
@endsection
