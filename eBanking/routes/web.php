<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('accounts', 'AccountController');
Route::post('/transaction', 'TransactionController@store');
Route::get('/transaction/create', 'TransactionController@create')->name('transaction.create');
Route::get('/transaction/{account}', 'TransactionController@show');
Route::get('/transactions/positive', 'TransactionController@transactionsPositive');
Route::get('/transactions/negative', 'TransactionController@transactionsNegative');
Route::get('/transactions', 'TransactionController@getAll');

Route::get('/transactions/reference/', 'TransactionController@getByReference');
Route::get('/transactions/purpose/', 'TransactionController@getByPurpose');
Route::get('/transactions/date/', 'TransactionController@getByDate');
Route::get('/transactions/value/', 'TransactionController@getByValue');


//Route::resource('transactions', 'TransactionController');
