<?php

use App\Role;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_employee = new Role();
        $role_employee->name = 'ROLE_EMPLOYEE';
        $role_employee->description = 'A Employee User';
        $role_employee->save();

        $role_employee = new Role();
        $role_employee->name = 'ROLE_CUSTOMER';
        $role_employee->description = 'A Customer User';
        $role_employee->save();

    }
}
