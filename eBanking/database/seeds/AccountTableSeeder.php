<?php

use App\User;
use Illuminate\Database\Seeder;
use App\Account;

class AccountTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $employerAccount = new Account();
        $employerAccount->account_number = env('BANK_NUMBER') . 1234567896;
        $employerAccount->bank_balance = 1000000000000;
        $employerAccount->iban = env('BANK_IBAN') . 12345675;
        $employerAccount->bic = env('BANK_BIC');
        $employerAccount->user_id = User::where('name', 'Mitarbeiter')->first()->id;
        $employerAccount->save();


        $account1 = new Account();
        $account1->account_number = env('BANK_NUMBER') . 1234567891;
        $account1->bank_balance = 0;
        $account1->iban = env('BANK_IBAN') . 12345678;
        $account1->bic = env('BANK_BIC');
        $account1->user_id = User::where('name', 'Fabi')->first()->id;
        $account1->save();

        $account2 = new Account();
        $account2->account_number = env('BANK_NUMBER') . 1234567892;
        $account2->bank_balance = 0;
        $account2->iban = env('BANK_IBAN') . 12345679;
        $account2->bic = env('BANK_BIC');
        $account2->user_id = User::where('name', 'Andi')->first()->id;
        $account2->save();
    }
}

