<?php

use App\Role;
use App\User;
use App\Account;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_employee = Role::where('name', 'ROLE_EMPLOYEE')->first();

        $employee = new User();
        $employee->name = 'Mitarbeiter';
        $employee->email = 'ma@kb.at';
        $employee->password = bcrypt('admin');
        $employee->save();
        $employee->roles()->attach($role_employee);

        $role_customer = Role::where('name', 'ROLE_CUSTOMER')->first();
        $customer1 = new User();
        $customer1->name = 'Fabi';
        $customer1->email = 'fabi@fabi.com';
        $customer1->password = bcrypt('fabi1234');
        $customer1->save();
        $customer1->roles()->attach($role_customer);

        $customer2 = new User();
        $customer2->name = 'Andi';
        $customer2->email = 'andi@andi.com';
        $customer2->password = bcrypt('idna1234');
        $customer2->save();
        $customer2->roles()->attach($role_customer);



    }
}
