<?php

use App\User;
use Illuminate\Database\Seeder;
use App\Account;
use App\Transaction;

class TransactionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userFabi = User::where('name', 'Fabi')->first();
        $userAndi = User::where('name', 'Andi')->first();

        $accountFabi = Account::where('user_id', $userFabi->id)->first();
        $accountAndi = Account::where('user_id', $userAndi->id)->first();



        for($i = 0; $i < 10; $i++){
            $transaction = new Transaction();
            $transaction->purpose = 'TestFabi2Andi' . $i;
            $transaction->reference = 'TestReference' . $i;
            $transaction->value = 100 + $i;
            $transaction->purpose = 'Test' . $i;
            $transaction->origin_id = $accountFabi->id;
            $transaction->receiver_id = $accountAndi->id;
            $transaction->save();
        }

        for($i = 0; $i < 10; $i++){
            $transaction = new Transaction();
            $transaction->purpose = 'TestAndi2Fabi' . $i;
            $transaction->reference = 'TestReference' . $i;
            $transaction->value = 200 + $i;
            $transaction->purpose = 'Test' . $i;
            $transaction->origin_id = $accountAndi->id;
            $transaction->receiver_id = $accountFabi->id;
            $transaction->save();
        }
    }
}
